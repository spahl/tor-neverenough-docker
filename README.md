# Intro

All tools from the paper [Once is Never Enough: Foundations for Sound
Statistical Inference in Tor Network
Experimentation](https://www.robgjansen.com/publications/neverenough-sec2021.pdf)
in a neat docker container. It makes the setup process a bit easier.

*Note*: For newer experiments you should use the current stable version of Shadow (3.0).

## Build image for neverenough

```sh
$ make build
```

## Do an experiment

**HINT:**: All `docker` commands will be executed without `sudo`! Add yourself
to `docker` group or use `sudo` before every command!

### Simulation of a 0.1% Tor network

```sh
$ cd ./experiments/active-links-0.3.5.18
```

Build the docker image, download needed files, to the simulation and analyse:

```sh
$ make SCALE=0.001 MONTH=2021-11 build
$ make SCALE=0.001 MONTH=2021-11 init
$ make SCALE=0.001 MONTH=2021-11 stage
$ make SCALE=0.001 MONTH=2021-11 generate
$ make SCALE=0.001 MONTH=2021-11 simulate
$ make SCALE=0.001 MONTH=2021-11 analyse
```

### Statistics of an example run:

```
start 18:37:52
end   19:10:16

Simulation time 60min
Memory usage    4.5GB
Needed data     53MB  (compressed)
Needed data     1.9GB (uncompresed)
```

## Paper

We use this archive for two cases in our [paper](https://gitlab.com/spahl/hydra-popets2023):

* performance measurements of two Tor versions [vanilla](https://gitlab.com/spahl/tor/-/tree/vanilla) and [pctls](https://gitlab.com/spahl/tor/-/tree/pctls) in a 25% scale Tor network.
  Each Tor version is measured in a network with and without packet loss ten times.
  This leads to four different configurations.
* measure the number of active shared links per second in a 25% scale Tor network.
  We log this data in the normal Tor log file.

We use the plotting function from `tornettools`, but we customize it for our use case, see [notebook](https://gitlab.com/spahl/hydra-popets2023/-/blob/main/analysis/paper-performance.ipynb).

### Setup

```sh
$ cd experiments/active-links-0.3.5.18
$ mkdir -p data/
```

Make directories for every configuration:

```sh
$ mkdir data/0.25-kist
$ mkdir data/0.25-kist-loss
$ mkdir data/0.25-kist-pctcp
$ mkdir data/0.25-kist-pctcp-loss
```

Notes:

* `0.25-kist-pctcp*` referes to [pctls](https://gitlab.com/spahl/tor/-/tree/pctls)
* `*-loss` means with packet loss

#### To switch between `vanilla` and `pctls`:

```sh
$ cd tor
$ git checkout vanilla # or
$ git checkout pctls
$ cd ..
```

#### To switch between packet loss and without packet loss model:

Change the following line in `experiments/active-links-0.3.5.18/Makefile` from:

```
generate: tmodel-ccs2018.github.io/data/shadow/network/atlas.201801.shadow113.graphml.xml
```

to:

```
generate: tmodel-ccs2018.github.io/data/shadow/network/atlas-lossless.201801.shadow113.graphml.xml
```

#### Experiment

```sh
$ make SCALE=0.25 MONTH=2021-11 build
$ make SCALE=0.25 MONTH=2021-11 init
$ make SCALE=0.25 MONTH=2021-11 stage
$ make SCALE=0.25 MONTH=2021-11 generate
$ make SCALE=0.25 MONTH=2021-11 simulate
$ make SCALE=0.25 MONTH=2021-11 analyse
$ make SCALE=0.25 MONTH=2021-11 archive
```

Move `tornet-0.25` to one of the four directories `data/{0.25-kist,0.25-kist-loss,0.25-kist-pctcp,0.25-kist-pctcp-loss}`, depending on the configuration, then you can start another experiment.
Every experiment will take 3 days, with the following system:

* Debian 11
* Linux Kernel 5.10.106-1.
* 4x16-Core AMD Opteron 6380
* 900 GB of RAM.

All forty experiments will take 4 months and require around 240 GB of storage.
Experiments will use up to 600 GB of RAM.

# References

* <https://neverenough-sec2021.github.io>
* <https://shadow.github.io/docs/guide/supported_platforms.html>
* <https://github.com/shadow/shadow>
* <https://github.com/shadow/shadow-plugin-tor>
* <https://github.com/shadow/oniontrace>
* <https://github.com/shadow/tgen>
* <https://github.com/shadow/tornettools>

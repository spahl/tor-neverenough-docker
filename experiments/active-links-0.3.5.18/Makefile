.PHONY: test build init stage generate simulate analyse

SCALE=0.01
MONTH=2020-11

START=${MONTH}-01
END=$(shell date +"%Y-%m-%d" -d "yesterday ${START} + 1 month")

export PATH := ../../bin:$(PATH)
# All scripts in `../../bin` use EXPERIMENT for the correct container
export EXPERIMENT := experiment-active-links:latest

test:
	echo ${EXPERIMENT}
	tornettools --version
	tor --version

build:
	git clone -b vanilla https://gitlab.com/spahl/tor || true
	docker build --network host -t ${EXPERIMENT} .

init:
	wget -nc https://collector.torproject.org/archive/relay-descriptors/consensuses/consensuses-${MONTH}.tar.xz
	wget -nc https://collector.torproject.org/archive/relay-descriptors/server-descriptors/server-descriptors-${MONTH}.tar.xz
	wget -nc https://metrics.torproject.org/userstats-relay-country.csv
	wget -nc https://collector.torproject.org/archive/onionperf/onionperf-${MONTH}.tar.xz
	wget -nc -O bandwidth-${MONTH}.csv "https://metrics.torproject.org/bandwidth.csv?start=${START}&end=${END}" || true
	tar --skip-old-files -xaf consensuses-${MONTH}.tar.xz
	tar --skip-old-files -xaf server-descriptors-${MONTH}.tar.xz
	tar --skip-old-files -xaf onionperf-${MONTH}.tar.xz
	git clone https://github.com/tmodel-ccs2018/tmodel-ccs2018.github.io.git || true

stage:
	tornettools stage \
	consensuses-${MONTH} \
	server-descriptors-${MONTH} \
	userstats-relay-country.csv \
	--onionperf_data_path onionperf-${MONTH} \
	--bandwidth_data_path bandwidth-${MONTH}.csv \
	--geoip_path /home/tool/tor/src/config/geoip

generate: tmodel-ccs2018.github.io/data/shadow/network/atlas.201801.shadow113.graphml.xml
	tornettools generate \
	relayinfo_staging_${START}--${END}.json \
	userinfo_staging_${START}--${END}.json \
	tmodel-ccs2018.github.io \
	--atlas $< \
	--network_scale ${SCALE} \
	--prefix tornet-${SCALE}

simulate:
	tornettools simulate tornet-${SCALE}

analyse:
	tornettools parse tornet-${SCALE}
	tornettools plot \
		tornet-${SCALE} \
		--tor_metrics_path tor_metrics_${START}--${END}.json \
		--prefix pdfs-${SCALE}

archive:
	tornettools archive tornet-${SCALE}

tmodel-ccs2018.github.io/data/shadow/network/atlas.201801.shadow113.graphml.xml:
	unxz $@.xz

tmodel-ccs2018.github.io/data/shadow/network/atlas-lossless.201801.shadow113.graphml.xml:
	unxz $@.xz
